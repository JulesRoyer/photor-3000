#Programme traitement d'images
'''
 ____     __                  __                                   __        __        __        __
/\  _`\  /\ \                /\ \__                              /'__`\    /'__`\    /'__`\    /'__`\
\ \ \L\ \\ \ \___      ___   \ \ ,_\    ___    _ __             /\_\L\ \  /\ \/\ \  /\ \/\ \  /\ \/\ \
 \ \  __/ \ \  _ `\   / __`\  \ \ \/   / __`\ /\`'__\           \/_/_\_<_ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \
  \ \ \/   \ \ \ \ \ /\ \L\ \  \ \ \_ /\ \L\ \\ \ \/              /\ \L\ \ \ \ \_\ \ \ \ \_\ \ \ \ \_\ \
   \ \_\    \ \_\ \_\\ \____/   \ \__\\ \____/ \ \_\              \ \____/  \ \____/  \ \____/  \ \____/
    \/_/     \/_/\/_/ \/___/     \/__/ \/___/   \/_/               \/___/    \/___/    \/___/    \/___/
'''
from tkinter import *
from tkinter.filedialog import askopenfilename, asksaveasfilename
from math import *
from PIL import Image, ImageDraw, ImageTk
from random import *
from threading import Thread

#_______________________________________________________________________________________________________
#classes________________________________________________________________________________________________
class Cont(Thread):
    """cree 2 listes de contour en partant d'une image en noir et blanc"""
    def __init__(self, image, bit):
        Thread.__init__(self)
        self.img = image
        self.lol = bit
        self.result = None

    def run(self):
        a=20
        controu=[]
        controu2=[]
        if self.lol:
            for i in range(self.img.size[1]): #en ligne (on parcourt l'image de gauche a droite)
                for j in range(self.img.size[0]-1):
                    A=self.img.getpixel((j,i))
                    Z=self.img.getpixel((j+1,i))        #on compare les valeurs entre deux pixels et si cette difference depasse a, la coordonnee du premier pixel est rangee dans une liste
                    cal=abs((A[0])-(Z[0]))
                    if cal>a:
                        controu+=[(j,i)]
            self.result = controu
        else:
            for j in range(self.img.size[0]): #en colonnes (on parcourt l'image de haut en bas)
                for i in range(self.img.size[1]-1):
                    A=self.img.getpixel((j,i))
                    Z=self.img.getpixel((j,i+1))        #on compare les valeurs entre deux pixels et si cette difference depasse a, la coordonnee du premier pixel est rangee dans une liste
                    cal=abs((A[0])-(Z[0]))
                    if cal>a:
                        controu2+=[(j,i)]
            self.result = controu2
    def resultat(self):
        return self.result

class D3(Thread):
    """ revoie 2 images cyan et rouge compilées en une seule pour etre utilisée dans le module 3D"""
    def __init__(self, image1, image2, bit):
        Thread.__init__(self)
        self.img1=image1
        self.img2=image2
        self.lol=bit
        self.result=None

    def run(self):
        xx=5
        xx2=2                                  #ecart cyan/rouge pour l'objet
        img_C=Image.new(self.img1.mode, self.img1.size)
        img_R=Image.new(self.img1.mode, self.img1.size)
        img_3D=Image.new(self.img1.mode, self.img1.size)
        if self.lol:
            #Cyan fond
            for i in range(self.img1.size[1]):#en lignes
                for j in range(self.img1.size[0]-xx):#puis en colonnes
                    B=self.img2.getpixel((j,i))
                    img_C.putpixel((j+xx,i), (0,B[1],B[2]))
            #Rouge fond
            for i in range(self.img1.size[1]):
                for j in range(xx,self.img1.size[0]):
                    B=self.img2.getpixel((j,i))
                    img_R.putpixel((j-xx,i), (B[0],0,0))
            #compile les images cyan et rouge du fond avec un decalage precedemment defini dans les boucles precedentes
            for i in range(self.img1.size[1]):
                for j in range(10,self.img1.size[0]):
                    C=img_C.getpixel((j,i))
                    R=img_R.getpixel((j,i))
                    img_3D.putpixel((j,i), (R[0],C[1],C[2]))
        else:
            #Cyan objet
            for i in range(self.img1.size[1]):#en lignes
                for j in range(xx2,self.img1.size[0]):#puis en colonnes
                    B=self.img1.getpixel((j,i))
                    img_C.putpixel((j-xx2,i), (0,B[1],B[2]))
            #Rouge objet
            for i in range(self.img1.size[1]):
                for j in range(self.img1.size[0]-xx2):
                    B=self.img1.getpixel((j,i))
                    img_R.putpixel((j+xx2,i), (B[0],0,0))
            #compile les images cyan et rouge de l'objet avec un decalage precedemment defini dans les boucles
            for i in range(self.img1.size[1]):
                for j in range(self.img1.size[0]):
                    C=img_C.getpixel((j,i))
                    R=img_R.getpixel((j,i))
                    img_3D.putpixel((j,i), (R[0],C[1],C[2]))
        del img_C
        del img_R

        self.result=img_3D

    def resultat(self):
        return self.result
    

#_______________________________________________________________________________________________________
#fonctions______________________________________________________________________________________________
def funkyguy(img):
    global can, crack
    if crack:
        crack['text']='Refais tourner gros !'
    a=randint(0,2)
    b=randint(0,2)
    c=randint(0,2)
    img_in=Image.open(img)
    img_fun=Image.new(img_in.mode, img_in.size)
    for i in range(img_in.size[1]):
        for j in range(img_in.size[0]):
            G=img_in.getpixel((j,i))
            img_fun.putpixel((j,i), (G[a],G[b],G[c]))
    can=Canvas(fenetre, height=400, width=600, bg="black")
    larg1=img_fun.size[0]
    haut1=img_fun.size[1]
    can.image=ImageTk.PhotoImage(img_fun)
    can.create_image(larg1//2, haut1//2, image=can.image)
    can.grid(row=0, column=0, rowspan=40, columnspan=40, padx=10, pady=10)#affiche l'image obtenue
    savetemp(img_fun)
    
def noiretblanc(img):#noir et blanc---------------------------------------------
    global can
    img_in=Image.open(img)
    img_gris=Image.new(img_in.mode, img_in.size)
    for i in range(img_in.size[1]):
        for j in range(img_in.size[0]):
            G=img_in.getpixel((j,i))
            m=((G[0]+G[1]+G[2])//3)#fait une moyenne des valeur RVB de l'image pour ensuite l'appliquer
            img_gris.putpixel((j,i), (m,m,m))
    can=Canvas(fenetre, height=400, width=600, bg="black")
    larg1=img_gris.size[0]
    haut1=img_gris.size[1]
    can.image=ImageTk.PhotoImage(img_gris)
    can.create_image(larg1//2, haut1//2, image=can.image)
    can.grid(row=0, column=0, rowspan=40, columnspan=40, padx=10, pady=10)#affiche l'image obtenue
    savetemp(img_gris)#l'enregistre temporairement pour qu'elle puisse etre reutilisee

def negatif(img):#negatif-------------------------------------------------------
    global can
    img_in=Image.open(img)
    img_D=Image.new(img_in.mode, img_in.size, color=(0,0,0))
    for i in range(img_in.size[1]):
        for j in range(img_in.size[0]):
            G=img_in.getpixel((j,i))
            img_D.putpixel((j,i), (255-G[0],255-G[1],255-G[2]))#applique "l'inverse" des valeurs RVB actuelles de l'image
    larg1=img_D.size[0]
    haut1=img_D.size[1]
    can=Canvas(fenetre, height=400, width=600, bg="black")
    can.image=ImageTk.PhotoImage(img_D)
    can.create_image(larg1//2, haut1//2, image=can.image)
    can.grid(row=0, column=0, rowspan=40, columnspan=40, padx=10, pady=10)
    savetemp(img_D)
#-------------------------------------------------------------------------------
#detourage manuel---------------------------------------------------------------
def points(event):
    liste.append((event.x,event.y))#cree des points en croix et les relie par une ligne
    if len(liste)>1:
        for i in range (len(liste)-1):
            can.create_line(liste[i][0], liste[i][1], liste[i+1][0], liste[i+1][1], fill="red")
    for i in range (len(liste)):
        can.create_line(liste[i][0], liste[i][1], liste[i][0]-5,liste[i][1], fill="red")
        can.create_line(liste[i][0], liste[i][1], liste[i][0]+5,liste[i][1], fill="red")
        can.create_line(liste[i][0], liste[i][1], liste[i][0],liste[i][1]-5, fill="red")
        can.create_line(liste[i][0], liste[i][1], liste[i][0],liste[i][1]+5, fill="red")
    return

def manu(liste, img):
    img_in=Image.open(img)
    det=Image.new(img_in.mode, img_in.size, color=(0,0,0))
    det2=Image.new(img_in.mode, img_in.size, color=(0,0,0))
    polydraw = ImageDraw.Draw(det)
    polydraw.polygon(liste, fill=(255,255,255))         #cree un polygone blanc, qui sera remplace ensuite par l'objet detoure
    del polydraw
#cree deux images differentes sur fond noir, l'une de l'objet net detoure, l'autre du fond detache de l'objet
    for i in range(img_in.size[1]):#l'objet
        for j in range(img_in.size[0]):
            U=det.getpixel((j,i))
            U_in=img_in.getpixel((j,i))
            if U[0]==255 and U[1]==255 and U[2]==255:
                det.putpixel((j,i), (U_in[0], U_in[1], U_in[2]))
    for i in range(img_in.size[1]):#le fond
        for j in range(img_in.size[0]):
            U=det.getpixel((j,i))
            U_in=img_in.getpixel((j,i))
            if U[0]==0 and U[1]==0 and U[2]==0:
                det2.putpixel((j,i), (U_in[0], U_in[1], U_in[2]))
    le3D(det, det2)

def ledetourmanuel(img):
    global liste, can, continuer
    liste=[]
    can=Canvas(fenetre, height=400, width=600, bg="black")
    swa=Image.open(img)
    larg1=swa.size[0]
    haut1=swa.size[1]
    can.image=ImageTk.PhotoImage(swa)
    can.create_image(larg1//2, haut1//2, image=can.image)
    can.bind("<Button-1>", points)
    boutonauto.destroy()
    boutonmanuel['text']='Recommencer'
    boutonmanuel['command']=lambda:(ledetourmanuel(img))
    continuer=Button(fenetre, width=38, bd=3, relief="groove", text="Valider les points", command= lambda:())
    continuer['command']=lambda:(manu(liste, img))
    continuer.grid(row=5, column=40, columnspan=4, padx=5)
    can.grid(row=0, column=0, rowspan=40, columnspan=40, padx=10, pady=10)
#-------------------------------------------------------------------------------
def ledetourauto(img):#detourage auto-------------------------------------------
    img_in=Image.open(img)
    img_D=Image.new(img_in.mode, img_in.size, color=(0,0,0))
    img_D2=Image.new(img_in.mode, img_in.size, color=(0,0,0))
    img_E=Image.new(img_in.mode, img_in.size, color=(0,0,0))
    img_gris=Image.new(img_in.mode, img_in.size)
    for i in range(img_in.size[1]):
        for j in range(img_in.size[0]):
            G=img_in.getpixel((j,i))
            m=((G[0]+G[1]+G[2])//3)             #transforme l'image en niveaux de gris pour que toutes les couleurs aient la meme valeur au sein du meme pixel
            img_gris.putpixel((j,i), (m,m,m))

    thread1, thread2=Cont(img_gris, True ), Cont(img_gris, False)
    thread1.start()
    thread2.start()

    thread1.join()
    thread2.join()
    
    contour=thread1.resultat()
    contour2=thread2.resultat()

    draw, draw2 = ImageDraw.Draw(img_D), ImageDraw.Draw(img_E)
#on cree des lignes allant de la premiere coordonnee a la derniere,
#les lignes de gauche a droite sont magenta et les lignes de haut en bas verte, de telle maniere que les points d'intersections soient blanc, et donc remplaçables par l'objet net
    for k in range(len(contour)-1):
            draw.line((contour[k][0], contour[k][1], contour[k+1][0], contour[k+1][1]), fill=(255,0,255))
    for k in range(len(contour2)-1):
            draw2.line((contour2[k][0], contour2[k][1], contour2[k+1][0], contour2[k+1][1]), fill=(0,255,0))
    del draw2
    del draw
    for i in range(img_in.size[1]):
        for j in range(img_in.size[0]):
            R=img_E.getpixel((j,i))
            T=img_D.getpixel((j,i))
            img_D.putpixel((j,i), (T[0],R[1],T[2]))
    del img_E
#cree deux images differentes sur fond noir, l'une de l'objet net détoure, l'autre du fond detache de l'objet
    for i in range(img_in.size[1]):#l'objet
        for j in range(img_in.size[0]):
            U=img_D.getpixel((j,i))
            U_in=img_in.getpixel((j,i))
            if U[0]==255 and U[1]==0 and U[2]==255:#on elimine le magenta
                img_D.putpixel((j,i), (0,0,0))
            elif U[0]==0 and U[1]==255 and U[2]==0:#on elimine les vert
                img_D.putpixel((j,i), (0,0,0))
            elif U[0]==255 and U[1]==255 and U[2]==255:#on remplace le blanc par les valeurs RVB de l'objet net
                img_D.putpixel((j,i), (U_in[0], U_in[1], U_in[2]))
    for i in range(img_in.size[1]):#le fond
        for j in range(img_in.size[0]):
            U=img_D.getpixel((j,i))
            U_in=img_in.getpixel((j,i))
            if U[0]==0 and U[1]==0 and U[2]==0:
                img_D2.putpixel((j,i), (U_in[0], U_in[1], U_in[2]))
    le3D(img_D, img_D2)

#----------------------------------------------------------------------------------
def le3D(img_D, img_D2):#Module 3D-------------------------------------------------
    t1=D3(img_D, img_D2, True)
    t2=D3(img_D, img_D2, False)

    t1.start()
    t2.start()

    t1.join()
    t2.join()

    img_3Dobjet= t2.resultat()
    img_3Dfond= t1.resultat()
    
#compile les deux images de fond et d'objet pour n'en avoir qu'une finale
    for i in range( img_D.size[1]):
        for j in range(img_D.size[0]):
            ED=img_3Dfond.getpixel((j,i))
            RD=img_3Dobjet.getpixel((j,i))
            if RD[0]==0 and RD[1]==0 and RD[2]==0:
                img_3Dobjet.putpixel((j,i), (ED[0],ED[1],ED[2]))
            if RD[0]==255 and RD[1]==0 and RD[2]==0:
                img_3Dobjet.putpixel((j,i), (RD[0],ED[1],ED[2]))
            if RD[0]==0 and RD[1]==255 and RD[2]==255:
                img_3Dobjet.putpixel((j,i), (ED[0],RD[1],RD[2]))
            
    larg1=img_3Dobjet.size[0]
    haut1=img_3Dobjet.size[1]
    can=Canvas(fenetre, height=400, width=600, bg="black")
    can.image=ImageTk.PhotoImage(img_3Dobjet)
    can.create_image(larg1//2, haut1//2, image=can.image)
    can.grid(row=0, column=0, rowspan=40, columnspan=40, padx=10, pady=10)
    savetemp(img_3Dobjet)
#-------------------------------------------------------------------------------
def parcouropen():#parcourir----------------------------------------------------
    op=askopenfilename(filetypes=(("Image Files", "*.jpg;*.jpeg;*.gif"),("All files", "*.*")), defaultextension=".jpg")
    ouvrir_img(op)
    empp.set(op)
    
def parcoursauve():
    if empp.get()!="":#enregistre l'image par parcours des dossiers, en ajoutant .jpg à la fin du nom pour le fichier final si rien n'est entré comme autre extension
        hello=asksaveasfilename(filetypes=(("Image Files", "*.jpg;*.jpeg;*.gif"),("All files", "*.*")), defaultextension=".jpg")
        sauverfinal(hello)
        saving.set(hello)
    else :#si aucune image n'est ouverte, on ne peut pas enregistrer
        erreur=LabelFrame(fenetre, height=200, width=200, bd=5, relief="groove", text="Erreur")
        erreur1=Label(erreur, text="Vous n'avez pas ouvert d image !\nPour ouvrir une image, entrez son chemin d'acces\net validez")
        erreur.grid(row=5, column=40, columnspan=5)
        erreur1.grid()
        #global erreur
        
def sauverfinal(chemin):
    imager=Image.open(empp.get())
    imager.save(chemin)
    
def ouvrir_img(emp):#ouvre et affiche l'image
    can=Canvas(fenetre, height=400, width=600, bg="black")
    if emp!="":
        swa=Image.open(emp)
        larg1=swa.size[0]
        haut1=swa.size[1]
        can.image=ImageTk.PhotoImage(swa)
        can.create_image(larg1//2, haut1//2, image=can.image)
    can.grid(row=0, column=0, rowspan=40, columnspan=40, padx=10, pady=10)
    
def savetemp(img):#sauvegarde un fichier "temporaire" en fonction de son nom et de son extension
    aa=empp.get()
    if "-modifie-" in aa:
            img.save(aa)
    elif ".JPG" in aa:
        img.save(aa.split(".JPG")[0]+"-modifie-"+".jpg")
        empp.set(aa.split(".JPG")[0]+"-modifie-"+".jpg")
    elif ".jpg" in aa:
        img.save(aa.split(".jpg")[0]+"-modifie-"+".jpg")
        empp.set(aa.split(".jpg")[0]+"-modifie-"+".jpg")
    elif ".jpeg" in aa:
        img.save(aa.split(".jpeg")[0]+"-modifie-"+".jpeg")
        empp.set(aa.split(".jpeg")[0]+"-modifie-"+".jpeg")
    elif ".gif" in aa:
        img.save(aa.split(".gif")[0]+"-modifie-"+".gif")
        empp.set(aa.split(".gif")[0]+"-modifie-"+".gif")
    else:
        img.save("temp.jpg")
        empp.set("temp.jpg")

def bite():
    global boutonauto, boutonmanuel, continuer
    try:
        if boutonauto or boutonmanuel:
            boutonauto.destroy()
            boutonmanuel.destroy()
        if continuer:
            continuer.destroy()
        if crack:
            crack.destroy()
            lsd.destroy()
    except:
        ()

def partydance(emplacement, effet):#lance le module adapte et modifie l'interface en fonction du module choisi
    global erreur, crack, lsd, boutonauto, boutonmanuel
    try:
        if erreur:
            erreur.destroy()

    except:
        ()
    if effet=="3D" and emplacement!="":
        bite()
        try:
            if continuer:
                continuer.destroy()
        except:
            ()
        boutonauto=Button(fenetre, width=38, bd=3, relief="groove", text="Detourage automatique", command= lambda:())
        boutonmanuel=Button(fenetre, width=38, bd=3, relief="groove", text="Detourage manuel", command= lambda:())
        boutonauto['command']=lambda:(ledetourauto(emplacement))
        boutonmanuel['command']=lambda:(ledetourmanuel(emplacement))
        boutonmanuel.grid(row=4, column=40, columnspan=4, padx=5)
        boutonauto.grid(row=3, column=40, columnspan=4, padx=5)
    elif effet=='Negatif' and emplacement!="":
        bite()
        negatif(emplacement)
    elif effet=='Noir et blanc' and emplacement!="":
        bite()
        noiretblanc(emplacement)
    elif effet=='Funky Yeah' and emplacement!="":
        bite()
        crack=Button(fenetre, width=38, bd=3, relief="groove", text="Valider !", command= lambda:())
        lsd=LabelFrame(fenetre, height=200, width=200, bd=5, relief="groove", text="Info")
        weed=Label(lsd, text="Il est recommendé d'enregistrer son travail avant de lancer")
        crack['command']=lambda:(funkyguy(emplacement))
        lsd.grid(row=5, column=40, columnspan=5)
        weed.grid()
        crack.grid(row=8, column=40, columnspan=4, padx=5)
    elif emplacement=="":
        erreur=LabelFrame(fenetre, height=200, width=200, bd=5, relief="groove", text="Erreur")
        erreur1=Label(erreur, text="Vous n'avez pas ouvert d'image !\nPour ouvrir une image, entrez son chemin d'acces\net validez")
        erreur.grid(row=5, column=40, columnspan=5)
        erreur1.grid()
        

#________________________________________________________________________________________________________
#corps___________________________________________________________________________________________________

fenetre=Tk()
can=Canvas(fenetre, height=400, width=600, bg="black")
opening=Button(fenetre, width=10,text="Ouvrir !", bd=5, relief="ridge",command= lambda:())
save=Button(fenetre, width=10,text="Sauvegarder !", bd=5, relief="ridge",command= lambda:())
go=Button(fenetre, text="Go !", bd=4, relief="raised", width=38, command= lambda:())
parcourir1=Button(fenetre, width=10, text="Parcourir", command= lambda:())
parcourir2=Button(fenetre, width=10, text="Parcourir", command= lambda:())
empp=StringVar()
saving=StringVar()
savebar=Entry(fenetre, textvariable=saving, width=90)
openbar=Entry(fenetre, textvariable=empp, width=90)
sauvsous=Label(fenetre, text="Sauver sous :")
openfile=Label(fenetre, text="Ouvrir :")
listeOp=("Selection...",'_____________','3D', 'Negatif', 'Noir et blanc','Funky Yeah')
v = StringVar()
v.set(listeOp[0])
OpMenu= OptionMenu(fenetre, v, *listeOp)
Oplabel=Label(fenetre, text="Effet :")
go['command']=lambda:(partydance(empp.get(), v.get()))
opening['command']=lambda:(ouvrir_img(empp.get()))
save['command']=lambda:(sauverfinal(saving.get()))
parcourir1['command']=lambda:(parcouropen(), print("salut"))
parcourir2['command']=lambda:(parcoursauve())



openbar.grid(row=41, column=20, sticky=E)
savebar.grid(row=42, column=20, pady= 10, sticky=E)
sauvsous.grid(row=42, column=1)
openfile.grid(row=41, column=1)
parcourir1.grid(row=41, column=40)
parcourir2.grid(row=42, column=40)
save.grid(row=42, column=41)
opening.grid(row=41, column=41)
OpMenu.grid(row=0, column=41)
Oplabel.grid(row=0, column=40, sticky=NSEW)
can.grid(row=0, column=0, rowspan=40, columnspan=40, padx=10, pady=10)
go.grid(row=2, column=40, columnspan=4, padx=5)


fenetre.mainloop()
