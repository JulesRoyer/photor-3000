# Projet ISN - 2015
## Prérequis
* Python 3.x
* Tkinter
* Pillow
* Des images à tranfsormer
* Des lunettes 3D anaglyphes (cyan et rouge)

## Utilisation
Tout se passe par l'interface graphique : vous ouvrez votre image, vous cliquez sur le style de transformation à utiliser et vous appliquez.
Une fois une transformation appliquée, vous pouvez recommencer et en appliquer d'autres.
Lorsque le résultat vous plait vous pouvez enregistrer votre résultat dans le dossier de votre choix.

## Attention
J'ai programmé ce projet en Terminale, j'étais donc un novice en programmation, le code serait sûrement à restructurer et à améliorer.